<?php

// starten einer neuen Session
session_start();

?>
<?php

/**
 * @param $entry ist der Text, der vor dem jeweiligen Textfeld steht
 * @param $type Art des Textfeldes, nicht jedes Feld muss vom Typ text sein
 * @param $name, der Name mit dem das Textfeld in PHP angesprochen wird
 * @param string $value standartmäßig nicht ausgefühlt. Existiert eine get-Variable allerdings schon, wird sie automatisch
 * in das Feld als Wert eingefügt, damit der Nutzer dies nicht erneut tun muss
 */
function createTextField($entry, $type, $name, $value = "") {
    echo "<p>$entry: <input type='$type' name='$name' value='$value'></p>";
}

/**
 * Erstellt ein Textfeld bei dem mit dem letzten Parameter ein Text übergeben wird, der als Fehlermeldung erscheint.
 * @param $entry der Text der vor dem jeweiligen Textfeld steht
 * @param $type der Typ des Textfeldes
 * @param $name der Name des Textfeldes welcher dann mit PHP ausgewertet werden kann
 */
function createTextFieldWrong($entry, $type, $name, $text) {
    echo "<p>$entry: <input type='$type' name='$name' style='border:1px solid red'></span>
        <br><span style='color:red'> $text</span></p>";
}

/**
 * @param $type
 * @param $name
 * @param $text
 */
function createButton($type, $name, $text) {
    echo "<input type='$type' name='$name' value='$text'>";
}


/**
 * Methode prüft für einen übergebenen Text ob Sonderzeichen enthalten sind oder nicht
 * Wenn Sonderzeichen enthalten sind, gibt diese Methode true zurück, ansonsten false
 * @param $value
 */
function containsSpecialChars($value) {
    $pattern = "/[!?;,:+§=<>()]/";
    $contains = false;
    if (preg_match($pattern, $value)) {
        $contains = true;
    }
    return $contains;
}

/**
 * Methode prüft mit dem übergebenen Parameter ob dieser Wert aus genau 5 Zahlen besteht, danach ein Leehrzeichen und danach
 * eine Stadt, die aus mind. 2 Buchstaben besteht. Ist das der Fall wird die Variable $contains auf true gesetzt.
 * als PLZ erkannt.
 * @param $value
 * $return $contains
 */
function isPostCode($value) {
    $patter = "/(\d{5})\s([A-Za-z]{2,})/";
    $contains = false;
    if (preg_match($patter, $value))  {
        $contains = true;
    }
    return $contains;
}

/**
 * Die Methode prüft ob $value eine gültige Kombination aus Vor- und Nachname ist. Beide Namen dürfen nur die Buchstaben enthalten sowie die Umlaute.
 * Ausserdem ist auch der Bindestrich erlaubt.
 * @param $value
 * @return bool
 */
function isValideName($value) {
    $pattern = "/([A-Za-zÄÖÜäöü-]{2,})\s([A-Za-zÄÖÜäöü-]{2,})/";
    $contains = false;
    if (preg_match($pattern, $value)) {
        $contains = true;
    }
    return $contains;
}

/**
 * Die Methode prüft ob $value eine gültige Telefonnummer ist. Dabei muss die Nummer mindestens aus 7 Zahlen bestehen
 * (4 für die Vorwahl und min. 3 Ziffern für die lokale Nummer). Sie darf aber nicht länger sein als 23 Ziffern.
 * -- OLD VERSION--
 * @param $value
 * @return bool
 */
// function isValidePhoneNumber($value) {
//     $patter = "/[0-9]{7,23}/";
//     $contains = false;
//     if (preg_match($patter, $value)) {
//         $contains = true;
//     }
//     //return $contains;
// }

/**
 * Die Methode prüft ob $value eine gültige Telefonnummer ist. Dabei muss die Nummer mindestens aus 7 Zahlen bestehen
 * (4 für die Vorwahl und min. 3 Ziffern für die lokale Nummer). Sie darf aber nicht länger sein als 23 Ziffern.
 * -- NEW VERSION--
 * @param $value
 * @return bool
 */
function isValidePhoneNumber($value) {
    $patter = "/[0-9]{7,23}/";
    return preg_match($patter, $value) ? true : false;
}


/**
 * Erstellt die erste Seite mit den Feldern für die Wünsche. Die erste Seite wird dabei nur angezeigt, wenn alle drei get-Variablen noch keinen Wert haben.
 */
function createFirstPage() {

    if (empty($_GET['wish1']) || empty($_GET['wish2']) || empty($_GET['wish3'])
        && !isset($_GET['wish1']) || !isset($_GET['wish2']) || !isset($_GET['wish3'])) {
        echo "<h1>Meine Wünsche</h1>";
        echo "<form method='get' action='index.php'>";

        // Wenn auf den Submit Button gedrückt wurde
        if (isset($_GET['submitFirstPage'])) {

            if ($_GET['wish1'] == "" or empty($_GET['wish1'])) {
                createTextFieldWrong("Wunsch 1", "text", "wish1", "Bitte Feld ausfüllen!");
            } else if (containsSpecialChars($_GET['wish1'])) {
                createTextFieldWrong("Wunsch 1", "text", "wish1", "Bitte keine Sonderzeichen: !?;,:+§=<>()", $_GET['wish1']);
            } else if (isset($_GET['wish1']) || !empty($_GET['wish1'])) {
                createTextField("Wunsch 1", "text", "wish1", $_GET['wish1']);
            }


            if ($_GET['wish2'] == "" or empty($_GET['wish2'])) {
                createTextFieldWrong("Wunsch 2", "text", "wish2", "Bitte Feld ausfüllen!");
            } else if (containsSpecialChars($_GET['wish2'])) {
                createTextFieldWrong("Wunsch 2", "text", "wish2", "Bitte keine Sonderzeichen: !?;,:+§=<>()", $_GET['wish2']);
            } else if (isset($_GET['wish2'])) {
                createTextField("Wunsch 2", "text", "wish2", $_GET['wish2']);
            }


            if ($_GET['wish3'] == "" or empty($_GET['wish3'])) {
                createTextFieldWrong("Wunsch 3", "text", "wish3", "Bitte Textfeld ausfüllen!");
            } else if (containsSpecialChars($_GET['wish3'])) {
                createTextFieldWrong("Wunsch 3", "text", "wish3", "Bitte keine Sonderzeichen: !?;,:+§=<>()", $_GET['wish3']);
            } else if (isset($_GET['wish3'])) {
                createTextField("Wunsch 3", "text", "wish3", $_GET['wish3']);
            }



        } else {
            createTextField("Wunsch 1", "text", "wish1");
            createTextField("Wunsch 2", "text", "wish2");
            createTextField("Wunsch 3", "text", "wish3");
        }
        createButton("reset", "resetFirstPage", "Abbrechen");
        createButton("submit", "submitFirstPage", "OK");
        echo "</form>";
    }

}

/**
 * Erstellt die zweite Seite mit den Angaben zur Person
 */
function createSecondPage() {

    if (empty($_GET['name']) || empty($_GET['adress']) || empty($_GET['phone'])) {

        echo "<h1>Lieferanganben</h1>";
        echo "<p>1. Wunsch: " . $_SESSION['wish1'] . "Funktioniert</p>";
        echo "<p>2. Wunsch: " . $_SESSION['wish2'] . "</p>";
        echo "<p>3. Wunsch: " .  $_SESSION['wish3'] . "</p>";
        echo "<form method='get' action='index.php'>";

        if (isset($_GET['submitSecondPage'])) {

            if ($_GET['name'] == "" or empty($_GET['name'])) {
                createTextFieldWrong("Vor- und Nachname", "text", "name", "Bitte Feld ausfüllen");
            } else if (!isValideName($_GET['name'])) {
                createTextFieldWrong("Vor- und Nachname", "text", "name",
                    "Bitte gib Deinen Vor- und Nachnamen im richtigen Format ein! Z.B. Hans Werner", $_GET['name']);
            } else if (isset($_GET['name'])) {
                createTextField("Vor- und Nachname", "text", "name", $_GET['name']);
            }

            if ($_GET['adress'] == "" or empty($_GET['adress'])) {
                createTextFieldWrong("PLZ und Ort", "text", "adress", "Bitte Feld ausfüllen");
            } else if (!isPostCode($_GET['adress'])) {
                createTextFieldWrong("PLZ und Ort", "text", "adress", "Bitte geben Sie eine 5-stellige PLZ und einen Ort ein! Z.B. 11111 Auerbach", $_GET['adress']);
            } else if (isset($_GET['adress'])) {
                createTextField("PLZ und Ort", "text", "adress", $_GET['adress']);
            }

            if ($_GET['phone'] == "" or empty($_GET['phone'])) {
                createTextFieldWrong("Telefon", "text", "phone", "Bitte Feld ausfüllen");
            } else if (!isValidePhoneNumber($_GET['phone'])) {
                createTextFieldWrong("Telefon", "text", "phone", "Bitte geben Sie zwischen 7 und 23 Ziffern ein!");
            } else if (isset($_GET['phone'])) {
                createTextField("Telefon", "text", "phone", $_GET['phone']);
            }

        } else {
            createTextField("Vor- und Nachname", "text", "name");
            createTextField("PLZ und Ort", "text", "adress");
            createTextField("Telefon", "text", "phone");
        }
        createButton("reset", "deleteSecondPage", "Abbrechen");
        createButton("submit", "submitSecondPage", "OK");
        echo "</form>";
    }





}

/**
 * Erstellt die dritte Seite mit den Ergebnissen der ersten beiden Seiten
 */
function createThirdPage() {
    echo "<h1>Wunschübersicht</h1>";
    echo "<form method='get' action='../index.php'>";
    echo "<p>1. Wunsch: " . $_SESSION['wish1'] . "</p>";
    echo "<p>1. Wunsch: " . $_SESSION['wish2'] . "</p>";
    echo "<p>1. Wunsch: " . $_SESSION['wish3'] . "</p>";
    echo "<p>Vor- und Nachname: " . $_SESSION['name'];
    echo "<p>PLZ und Ort: " . $_SESSION['adress'] . "</p>";
    echo "<p>Telefon: " . $_SESSION['phone'] . "</p>";
    echo "</form>";

}


?>
<!DOCTYPE html>
<html>
<head>

    <style>
        * {
            box-sizing: border-box;
            font-family: sans-serif;
        }
        body, html {
            background: lightgray;
        }
        main {
            width: 960px;
            padding: 30px;
            margin: 0 auto;
            background: white;
        }

        input {
            margin-right: 10px;
        }

    </style>

</head>
<body>
<main>

    <?php


    /* Abfrage welche der drei Seiten angezeigt werden soll */

    /* Wenn die get-Parameter wish1, wish2 und wish3 noch leer sind soll die erste Seite aufgerufen werden. */
    if (empty($_GET['wish1']) || empty($_GET['wish2']) || empty($_GET['wish3'])
        && empty($_SESSION['wish1']) || empty($_SESSION['wish2']) || empty($_SESSION['wish3'])
        && !isset($_SESSION['wish1']) || !isset($_SESSION['wish2']) || !isset($_SESSION['wish3'])) {

        /* Diese Abfrage wird gebraucht, damit auf der dritten Seite nicht nochmal das Formular der ersten Seite angezeigt wird, weil die
            drei Variablen $_GET['wish1'] bis 3 nicht mehr existieren. Das Formular soll nur dann angezeigt werden wenn ausserdem auch die 3 Sessionvariablen
            nicht existieren*/
        if (empty($_SESSION['wish1']) || empty($_SESSION['wish2']) || empty($_SESSION['wish3'])) {
            /* Aufrufen der ersten Seite */
            createFirstPage();

        }
    }

    /* Wenn die get-Parameter wish1, wish2 und wish3 nicht mehr leer sind und auch die Session-Parameter gesetzt sind soll die 2. Ansicht erscheinen */
    if (isset($_GET['wish1']) && isset($_GET['wish2']) && isset($_GET['wish3'])
        && $_GET['wish1'] != "" && $_GET['wish2'] != "" && $_GET['wish3'] != ""
        || isset($_SESSION['wish1']) && isset($_SESSION['wish2']) && isset($_SESSION['wish3'])
        ) {
        if (empty($_SESSION['wish1']) && empty($_SESSION['wish2']) && empty($_SESSION['wish3'])) {
            $_SESSION['wish1'] = $_GET['wish1'];
            $_SESSION['wish2'] = $_GET['wish2'];
            $_SESSION['wish3'] = $_GET['wish3'];
        }


        /* Aufrufen der zweiten Seite */
        createSecondPage();
    }


    if (isset($_GET['name']) && isset($_GET['adress']) && isset($_GET['phone'])
            && $_GET['name'] != "" && $_GET['adress'] != "" && $_GET['phone'] != "") {

        $_SESSION['name'] = $_GET['name'];
        $_SESSION['adress'] = $_GET['adress'];
        $_SESSION['phone'] = $_GET['phone'];

        /* Aufrufen der dritten Seite */
        createThirdPage();
    }

    ?>

</main>

</body>
</html>
